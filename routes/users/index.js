/**
 * This files declares the users routes
 */

const Users = require('express').Router();
const Utils = require('../utils');

var usersController = require('./usersController.js');

// Users routes
Users.get('/userinfo/:id', Utils.loginRequired, usersController.getUserInfo);
Users.get('/confirmemail', usersController.confirmEmail);

Users.post('/login', usersController.loginUser);
Users.post('/register', usersController.registerUser);
Users.post('/recoverpassword', usersController.resetPassword);
Users.post('/resetpassword', usersController.confirmRecoverPassword);

Users.put('/edituserinfo', usersController.editUserInfo);

module.exports = Users;