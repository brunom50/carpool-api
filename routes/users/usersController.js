/**
 * This file is the controller for the users routes
 */

const usersDbMethods = require('./usersDbMethods');
const usersMethods = require('./usersMethods');
const nodeMailer = require('../modules/nodemailerModule');

const jwt = require('jsonwebtoken');
const smt = nodeMailer.init();
var randomstring = require("randomstring");

/**
 * This function is used to get the information of an user
 * @param req
 * @param res
 */
function getUserInfo(req, res) {
    let userId = req.params.id;

    usersDbMethods.getUserInfo(userId)
        .then(function (data) {
            if (data.received === 0) {
                return res.status(500).json({
                    success: false,
                    message: 'No user with the id provided was found'
                });
            }

            return res.status(200)
                .json({
                    status: 'success',
                    data: data
                });
        })
        .catch(function (err) {
            return res.status(500).json({
                success: false,
                message: err.message
            });
        });
}

/**
 * This function is used to update the info of an user
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function editUserInfo(req, res) {
    let userInfo = req.body;
    if (userInfo.id === undefined) {
        throw new Error('Please insert an user id');
    }

    return usersDbMethods.editUserInfo(userInfo)
        .then(function (result) {
            if(result.received === 0){
                throw new Error('No user with that id was found');
            }

            return res.json({success: true, message: 'User info of ' + result.id + ' was updated'});
        })
        .catch(function (err) {
            return res.status(500).json({success: false, message: err.message});
        });
}

/**
 * This function is used to perform a user login. First checks if the user with the username provided as parameter exists in the
 * database. If that its true, the function checks if the password provided and the hash retrieved in the database are a match. If that
 * its true, the login is successful.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function loginUser(req, res) {
    let userParams = {
        email: req.body.email,
        password: req.body.password,
    };

    return usersDbMethods.checkIfEmailExists(userParams.email)
        .then(function (user) {
            if (user.received === 0) {
                throw new Error('User not found');
            }

            if (user.email_confirmed !== true) {
                throw new Error('Please confirm your email.');
            }

            return usersMethods.compareHash(userParams.password, user.password);
        }).then(function (same) {
            if (!same) {
                throw new Error('Wrong password provided');
            }

            return res.json({
                success: true,
                token: jwt.sign({
                    username: userParams.username,
                    password: userParams.password
                }, 'CARPOOL')
            });
        }).catch(function (err) {
            return res.status(500).json({
                success: false,
                message: err.message
            });
        })
}

/**
 * This function is used to insert a new user in the database. First checks if the username or email already exists in the database.
 * Then, creates an hash of the password inserted by the user, and then calls the function that will insert the data in the database.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function registerUser(req, res) {
    let userParams = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    };

    let validations = usersMethods.validateUserFields(userParams);
    let checkPasswordStrength = usersMethods.validatePasswordStrength(userParams.password);

    if (checkPasswordStrength.length !== 0) {
        return res.status(500).json({sucess: false, error: checkPasswordStrength});
    }

    if (validations) {
        return res.status(500).json({sucess: false, error: validations.message});
    }

    return usersDbMethods.checkIfUserExists(userParams)
        .then(function (user) {
            if (user.length !== 0) {
                throw new Error('User already registered');
            }

            return usersMethods.hashPassword(userParams.password);
        }).then(function (hash) {
            userParams.password = hash;

            return usersDbMethods.insertNewUser(userParams);
        }).then(function (user) {
            var token = jwt.sign({
                username: user.username,
                email: user.email
            }, 'CARPOOL CONFIRMATION EMAIL');

            var data = {
                to: user.email,
                from: 'bruno.50.ramos@gmail.com',
                template: 'confirmationEmail',
                subject: 'Welcome to Carpool',
                context: {
                    url: 'http://localhost:3000/auth/confirmemail?token=' + token,
                    name: user.username
                }
            };

            smt.sendMail(data, function (err) {
                if (err) {
                    return res.json({message: err.message});
                }

                return res.status(400).json({sucess: true, message: 'User inserted with success with id: ' + user.id});
            });
        }).catch(function (err) {
            return res.status(500).json({sucess: false, error: err.message});
        });
}

/**
 * This function is used to recover a users password. First it checks if the email provided is associated with any user in
 * the database. If that is true, the function generates a token and a expiration data that will be stored in the database.
 * Finally, sends and email to the user with the reset password link.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function resetPassword(req, res) {
    let userParams = req.body;
    let userId;
    let password;

    return usersDbMethods.checkIfEmailExists(userParams.email)
        .then(function (user) {
            if (user.received === 0) {
                throw new Error('User does not exist');
            }

            userId = user.id;

            password = randomstring.generate(8);

            return usersMethods.hashPassword(password);
        })
        .then(function (hash) {
            return usersDbMethods.updateUserPassword(userId, hash);
        })
        .then(function (user) {
            var data = {
                to: user.email,
                from: 'bruno.50.ramos@gmail.com',
                template: 'recoverPassword',
                subject: 'Password help has arrived!',
                context: {
                    password: password,
                    name: user.username
                }
            };

            smt.sendMail(data, function (err) {
                if (err) {
                    return res.json({message: err.message});
                }
                return res.json({message: 'Kindly check your email for further instructions'});
            });
        })
        .catch(function (err) {
            return res.status(500).json({success: false, message: err.message});
        });
}

/**
 * This function is used to reset a users password. First checks if the token provided is associated with a user in the database.
 * After that checks the strength of the password provided. If there is no issue, the new password is stored in the db and
 * a email is sent to the user confirming the success in changing is password.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function confirmRecoverPassword(req, res) {
    let token = req.body.token;
    let password = req.body.password;

    return usersDbMethods.checkRecoveyToken(token)
        .then(function (user) {
            if (user.length === 0) {
                throw new Error('Token is invalid');
            }

            var checkPasswordStrength = usersMethods.validatePasswordStrength(password);

            if (checkPasswordStrength.length !== 0) {
                throw new Error(checkPasswordStrength);
            }

            return usersMethods.hashPassword(password);
        }).then(function (hash) {
            return usersDbMethods.resetUserPassword(hash);
        }).then(function (user) {
            if (user.length === 0) {
                throw new Error('Error resetting password');
            }

            var data = {
                to: user.email,
                from: 'bruno.50.ramos@gmail.com',
                template: 'confirmRecoverPassword',
                subject: 'Password Reset Confirmation',
                context: {
                    name: user.first_name + user.last_name
                }
            };

            smt.sendMail(data, function (err) {
                if (err) {
                    throw new Error(err);
                }

                return res.json({success: true, message: 'Password reset'});
            });
        })
        .catch(function (err) {
            return res.status(500).json({sucess: false, error: err.message});
        });
}

/**
 * This function is used to confirm a users email. First extracts the username and email from the jwt provided. After that
 * calls the function that will update the flag email_confirmed in the database to true.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function confirmEmail(req, res) {
    var token = req.query.token;

    var verifiedJwt = jwt.verify(token, 'CARPOOL CONFIRMATION EMAIL');

    if (verifiedJwt === undefined) {
        throw new Error('Invalid token provided');
    }

    return usersDbMethods.confirmEmail(verifiedJwt)
        .then(function (result) {
            if (result.id === undefined) {
                throw new Error('Error confirming email');
            }

            return res.status(200).json({sucess: true, message: 'Email confirmed of the user ' + result.id})
        }).catch(function (err) {
            return res.status(500).json({sucess: false, error: err.message});
        })
}

module.exports = {
    getUserInfo: getUserInfo,
    editUserInfo: editUserInfo,
    registerUser: registerUser,
    loginUser: loginUser,
    resetPassword: resetPassword,
    confirmRecoverPassword: confirmRecoverPassword,
    confirmEmail: confirmEmail
};
