/**
 * This file contains the utils methods for the users routes
 */

const bcrypt = require('bcrypt');
const validator = require('validator');
const passwordValidator = require('password-validator');

/**
 * This function validates if the password provided by the user matches the correct pattern and is a strong and secure
 * password.
 * @param {string] password - Password provided by the user
 * @returns {Array} errorMessages - Array containing all the errors messages
 */
function validatePasswordStrength(password){
    var schema = new passwordValidator();
    var errorMessages = [];
    var errors;

    schema
        .is().min(8)
        .is().max(25)
        .has().uppercase()
        .has().digits();

    errors = schema.validate(password, { list: true });

    if(errors.includes('uppercase')){
        errorMessages.push('Password must contain at least one uppercase letter');
    }

    if(errors.includes('min')){
        errorMessages.push('Password must contain at least 8 characters');
    }

    if(errors.includes('max')){
        errorMessages.push('Password must contain an maximum of 25 characters');
    }

    if(errors.includes('digits')){
        errorMessages.push('Password must contain at least 1 number');
    }

    return errorMessages;
}

/**
 * This function is used to create a hash of a string provides as parameter using bcrypt.
 * @param {string} password - Password inserted by the user
 * @returns {Promise.<TResult>} - Error or hash of the password
 */
function hashPassword(password){
    return bcrypt.hash(password, 10).then(function (err, hash) {
        if (err){
            return err;
        }

        return hash;
    })
}

/**
 * This function is used to compare a password and a hash using bcrypt.
 * @param {string} password - Password inserted by the user
 * @param {string} hash - Hash retrieved in the database
 */
function compareHash(password, hash) {
    return bcrypt.compare(password, hash);
}

/**
 * This function is used to validate if the users fields information are in the correct params.
 * @param {object} userParams - Object containing the user information.
 * @returns {Error} - Error message dependent of the validation that failed
 */
function validateUserFields(userParams) {
    if (!validator.isEmail(userParams.email)) {
        return new Error('Invalid email provided');
    }
}

module.exports = {
   hashPassword: hashPassword,
    compareHash: compareHash,
    validateUserFields: validateUserFields,
    validatePasswordStrength: validatePasswordStrength
};