/**
 * This file contains the db methods of the users routes
 */

var promise = require('bluebird');

var options = {
    promiseLib: promise
};

var pgp = require('pg-promise')(options);
const PS = pgp.PreparedStatement;

var connectionString = 'postgres://postgres:root@localhost:5433/carpool';
var db = pgp(connectionString);


/**
 * This function is used to get the info of an user.
 * @param {string} userId - User id
 * @returns {Promise<any[]>}
 */
function getUserInfo(userId) {
    const getUserInfo = new PS('getUserInfo', 'SELECT first_name, last_name,username, password, email ' +
        'FROM users WHERE id = $1');
    var params = [userId];

    getUserInfo.values = params;

    return db.one(getUserInfo)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to query the database and update the user info. The query is built dynamically depending of the
 * info that the user wants to update.
 * @param {object} userInfo - Object containing all the user info that is going to be edited
 * @returns {Promise.<TResult>}
 */
function editUserInfo(userInfo) {
    let params = [];
    let query = 'UPDATE users SET ';
    let regex = /,\s*$/;

    if (userInfo.first_name) {
        params.push(userInfo.first_name);
        query += 'first_name' + ' = $' + params.length + ', ';
    }

    if (userInfo.last_name) {
        params.push(userInfo.last_name);
        query += 'last_name' + ' = $' + params.length + ', ';
    }

    if (userInfo.username) {
        params.push(userInfo.username);
        query += 'username' + ' = $' + params.length + ', ';
    }

    query = query.replace(regex, "");

    params.push(userInfo.id);
    query += (' WHERE id = $' + params.length + ' RETURNING id');

    const preparedQuery = new PS('editUserInfo', query);
    preparedQuery.values = params;

    return db.one(preparedQuery)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is uses to check if the user related to the info provided, exists in the database.
 * @param {object} userInfo - Object with the new user info
 * @returns {Promise<any[]>}
 */
function checkIfUserExists(userInfo) {
    const checkUser = new PS('checkUser', 'SELECT id FROM users WHERE username = $1 OR email = $2');
    var params = [userInfo.username, userInfo.email];

    checkUser.values = params;

    return db.any(checkUser)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is uses to check if the username provided exists in the database.
 * @param {string} email - String with the email provided
 * @returns {Promise<any[]>}
 */
function checkIfEmailExists(email) {
    const checkUsername = new PS('checkUser', 'SELECT * FROM users WHERE email = $1');
    var params = [email];

    checkUsername.values = params;

    return db.one(checkUsername)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to insert the new user in the database.
 *
 * @param {object} userInfo - Object with the new user info
 * @returns {Promise.<TResult>}
 */
function insertNewUser(userInfo) {
    const addUser = new PS('addUser', 'INSERT INTO users(username, password, email, email_confirmed) ' +
        'VALUES ($1, $2, $3, $4) RETURNING *');

    var params = [userInfo.username, userInfo.password, userInfo.email, false];

    addUser.values = params;

    return db.one(addUser)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to updates the recover password token in the database.
 * @param {string} userId - Id of the user that is requesting to recover is password
 * @param {string} password - New password that will be stored in the database
 * @returns {Promise.<TResult>}
 */
function updateUserPassword(userId, password) {
    const query = new PS('updatePassword', 'UPDATE users SET password = $2 WHERE id = $1 RETURNING *');

    var params = [userId, password];

    query.values = params;

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to check if any user in the database has the token provided associated.
 * @param {string} token - Token that will be stored in the database
 * @returns {Promise.<TResult>}
 */
function checkRecoveyToken(token) {
    const query = new PS('checkRecoveyToken', 'SELECT * FROM users WHERE recover_password_token = $1');
    var params = [token];

    query.values = params;

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to reset the password of a user.
 * @param {string} hash - Hash of the new password that will be stored in the database.
 * @returns {Promise.<TResult>}
 */
function resetUserPassword(hash) {
    const query = new PS('resetUserPssword', 'UPDATE users SET password = $1, recover_password_token = $2, token_expiration = $3' +
        ' RETURNING *');

    var params = [hash, undefined, undefined];

    query.values = params;

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to confirm the users email. It sets a flag to true if the user clicks the confirmation link.
 * @param {object} userParams - Object containing the username and email extracted from the token
 * @returns {Promise.<TResult>}
 */
function confirmEmail(userParams) {
    const query = new PS('confirmEmail', 'UPDATE users SET email_confirmed = $1 WHERE username = $2 AND email = $3 RETURNING id');

    var params = [true, userParams.username, userParams.email];

    query.values = params;

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

module.exports = {
    confirmEmail: confirmEmail,
    editUserInfo: editUserInfo,
    getUserInfo: getUserInfo,
    updateUserPassword: updateUserPassword,
    checkIfUserExists: checkIfUserExists,
    insertNewUser: insertNewUser,
    checkIfEmailExists: checkIfEmailExists,
    checkRecoveyToken: checkRecoveyToken,
    resetUserPassword: resetUserPassword
};