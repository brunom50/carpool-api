/**
 * This file contains the utils methods used by all the routes
 */

'use strict';

/**
 * This function prevents unauthorized requests from being made
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function loginRequired(req, res, next) {
    if (req.user) {
        return next();
    }
    return res.status(401).json({message: 'Unauthorized user!'});
}

module.exports = {
    loginRequired: loginRequired
};