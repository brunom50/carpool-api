/**
 * This file is were the main routes are declared
 */

const Routes = require('express').Router();
var jwt = require('jsonwebtoken');

/**
 * This functions used to validate if the the api request bring a valid jwt token. This prevents unauthorized access.
 */
Routes.use(function (req, res, next) {
    req.user = undefined;

    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
        jwt.verify(req.headers.authorization.split(' ')[1], 'CARPOOL', function (err, decode) {
            if (err) req.user = undefined;
            req.user = decode;
        });
    }

    return next();
});

//Defining users routes
const Users = require('./users');
Routes.use('/users', Users);

//Defining groups routes
const Groups = require('./groups');
Routes.use('/groups', Groups);

//Defining rides routes
const Rides = require('./rides');
Routes.use('/rides', Rides);

module.exports = Routes;
