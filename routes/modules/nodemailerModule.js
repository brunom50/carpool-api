/**
 * This file contains the configurations for the node mailer module. This module can be used to send emails.
 */

const hbs = require('nodemailer-express-handlebars'),
    email = process.env.MAILER_EMAIL_ID || 'bruno.50.ramos@gmail.com',
    pass = process.env.MAILER_PASSWORD || 'bii color5';

const nodemailer = require('nodemailer');

/**
 * This function initiates the smtpTrasnsport that will be used to send emails.
 */
function init() {
    let smtpTransport = nodemailer.createTransport({
        service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
        auth: {
            user: email,
            pass: pass
        }
    });

    let handlebarsOptions = {
        viewEngine: 'handlebars',
        viewPath: ('./public/templates'),
        extName: '.html'
    };

    smtpTransport.use('compile', hbs(handlebarsOptions));

    return smtpTransport;
}

module.exports = {
    init: init
};