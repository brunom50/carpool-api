/**
 * This file is the controller for the rides routes
 */

const ridesDbMethods = require('./ridesDbMethods');
const ridesMethods = require('./ridesMethods');

const datesBetween = require('dates-between');
var moment = require('moment-business-days');


/**
 * This function is used to create a ride and get all the information like distance, travel time, etc.
 * @param req
 * @param res
 * @returns {*}
 */
function createRide(req, res) {
    let ridesParams = req.body;

    if (ridesParams.origin === undefined || ridesParams.destination === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid origin and destination'});
    }

    if (ridesParams.arrivalDate === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid arrival date and time'});
    }

    return ridesMethods.getTravelInfo(ridesParams)
        .then(function (result) {
            return res.status(200).json(result);
        })
        .catch(function (err) {
            return res.status(500).json(err);
        })
}

/**
 * This function is used to create a new ride
 * @param req
 * @param res
 * @returns {*}
 */
function insertRide(req, res) {
    let ridesParams = req.body;

    if (ridesParams.origin === undefined || ridesParams.destination === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid origin and destination'});
    }

    if (ridesParams.arrivalDate === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid arrival date and time'});
    }

    return ridesMethods.getTravelInfo(ridesParams)
        .then(function (rideInfo) {
            return ridesDbMethods.insertRide(ridesParams.group_id, rideInfo);
        })
        .then(function (result) {
            if (result.id === undefined) {
                throw new Error('Error inserting ride');
            }

            return res.status(200).json({success: true, message: 'Ride inserted with id: ' + result.id});
        })
        .catch(function (err) {
            return res.status(500).json(err.message);
        })
}

/**
 * This function creates a scale of rides from the parameters introduced by the user. Firsts gets the information to thar ride.
 * Then checks the business days between the dates the user introduced and finally inserts the rides into the database.
 * If an error exists, the scale and the rides associated are deleted from the database.
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function createScaleOfRides(req, res) {
    let scaleParams = req.body;

    let startDate = new Date(scaleParams.startDate);
    let endDate = new Date(scaleParams.endDate);
    let group_id = scaleParams.group_id;
    let scale_id;

    let dates = [];

    for (const date of datesBetween(startDate, endDate)) {
        if (moment(date, 'DD-MM-YYYY').isBusinessDay()) {
            dates.push(date);
        }
    }

    return ridesDbMethods.insertScale(group_id)
        .then(function (id) {
            scale_id = id.id;
            return ridesMethods.getTravelInfo(scaleParams)
        })
        .then(function (info) {
            dates.forEach(function () {
                return ridesDbMethods.insertRide(group_id, scale_id, info);
            })
        })
        .then(function () {
            return res.status(200).json({success: true, message: dates});
        })
        .catch(function (err) {
            return ridesDbMethods.deleteScale(scale_id)
                .then(function () {
                    return res.status(500).json({success: false, message: err.message});
                })
        });
}

/**
 * This function is used to get the information of an determined ride
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function getRideInfo(req, res) {
    let rideId = req.body.rideId;

    if(rideId === undefined){
        throw new Error('No ride with that id was found');
    }

    return ridesDbMethods.getRideInfo(rideId)
        .then(function (rideInfo) {
            if (rideInfo.error) {
                throw new Error('No group was found with that id');
            }

            return res.status(200).json({success: true, message: rideInfo});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

function deleteRide(req, res) {
    let rideId = req.body.rideId;

    if(rideId === undefined){
        throw new Error('No ride with that id was found');
    }

    return ridesDbMethods.deleteRide(rideId)
        .then(function (rideInfo) {
            if (rideInfo.error) {
                throw new Error('No ride was found with that id');
            }

            return res.status(200).json({success: true, message: 'The ride was deleted'});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

function deleteRideScale(req, res){
    let rideScaleId = req.body.rideScaleId;

    if(rideScaleId === undefined){
        throw new Error('No ride scale id was inserted');
    }

    return ridesDbMethods.deleteRideScale(rideScaleId)
        .then(function (rideScaleInfo) {
            if (rideScaleInfo.error) {
                throw new Error('No ride scale was found with that id');
            }

            return res.status(200).json({success: true, message: 'The ride scale was deleted'});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

function getRideScaleInfo(req, res){
    let rideScaleId = req.query.rideScaleId;

    if(rideScaleId === undefined){
        throw new Error('No ride scale with that id was found');
    }

    return ridesDbMethods.getRideScaleInfo(rideScaleId)
        .then(function (rideScaleInfo) {
            if (rideScaleInfo.error) {
                throw new Error('No ride scale was found with that id');
            }

            return res.status(200).json({success: true, message: rideScaleInfo});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

function editRide(req, res){
    let ridesParams = req.body;

    if (ridesParams.origin === undefined || ridesParams.destination === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid origin and destination'});
    }

    if (ridesParams.arrivalDate === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid arrival date and time'});
    }

    return ridesMethods.getTravelInfo(ridesParams)
        .then(function (rideInfo) {
            return ridesDbMethods.editRide(ridesParams.ride_id, rideInfo);
        })
        .then(function (result) {
            if (result.id === undefined) {
                throw new Error('Error updating ride');
            }

            return res.status(200).json({success: true, message: 'Ride updated with id: ' + result.id});
        })
        .catch(function (err) {
            return res.status(500).json(err.message);
        })
}

function editRideScale(req, res) {
    let ridesParams = req.body;

    if (ridesParams.origin === undefined || ridesParams.destination === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid origin and destination'});
    }

    if (ridesParams.arrivalDate === undefined) {
        return res.status(500).json({success: false, message: 'Insert a valid arrival date and time'});
    }

    return ridesMethods.getTravelInfo(ridesParams)
        .then(function (rideInfo) {
            return ridesDbMethods.editRideScale(ridesParams.scale_id, rideInfo);
        })
        .then(function (result) {
            if (result.length === 0) {
                throw new Error('Error updating scale ride');
            }

            return res.status(200).json({success: true, message: 'Scale updated'});
        })
        .catch(function (err) {
            return res.status(500).json(err.message);
        })
}

module.exports = {
    createRide: createRide,
    insertRide: insertRide,
    createScaleOfRides: createScaleOfRides,
    getRideInfo: getRideInfo,
    deleteRide: deleteRide,
    deleteRideScale: deleteRideScale,
    getRideScaleInfo: getRideScaleInfo,
    editRide: editRide,
    editRideScale: editRideScale
};
