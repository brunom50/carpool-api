/**
 * This file contains the db methods of the rides routes
 */
/**
 * This file contains the db methods of the groups routes
 */

var promise = require('bluebird');

var options = {
    promiseLib: promise
};

var pgp = require('pg-promise')(options);
const PS = pgp.PreparedStatement;

var connectionString = 'postgres://postgres:root@localhost:5433/carpool';
var db = pgp(connectionString);

/**
 * This function is used to insert a new rides group into the database
 * @param {string} group_id - Id of the group related to the ride
 * @param {object} rideInfo - Object containing the ride info
 * @returns {Promise.<TResult>}
 */
function insertRide(group_id, scale_id, rideInfo) {
    const query = new PS('insertRide', 'INSERT INTO rides (origin, destination, departure_time,' +
        ' arrival_time, distance, group_id, scale_id)' +
        'VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id');

    query.values = [rideInfo.origin, rideInfo.destination, rideInfo.startDate,
        rideInfo.arrivalDate, rideInfo.distanceValue, group_id, scale_id];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to insert a new scale of rides in the database
 * @param {string} group_id - Id of the group related to the ride
 * @returns {Promise.<TResult>}
 */
function insertScale(group_id) {
    const query = new PS('insertScale', 'INSERT INTO rides_scales (group_id) VALUES ($1) RETURNING id');

    query.values = [group_id];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function executes a batch of queries that deletes a scale of rides and the rides associated.
 * @param {string} scale_id - Id of the scale of rides
 */
function deleteScale(scale_id) {
    db.tx(t => {
        const q1 = t.none('DELETE FROM scale_rides WHERE id = $1', [scale_id]);
        const q2 = t.one('DELETE FROM rides WHERE scale_id = $1', [scale_id]);

        return t.batch([q1, q2]);
    })
        .then(data => {
            return data;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to query the database and get the information of a ride
 * @param {string} rideId - Id of the ride
 * @returns {Promise.<TResult>}
 */
function getRideInfo(rideId) {
    const query = new PS('getRideInfo', 'SELECT * FROM rides WHERE id = $1');

    query.values = [rideId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to delete a ride from the database
 * @param {string} rideId - Id of the ride to be deleted
 * @returns {Promise.<TResult>}
 */
function deleteRide(rideId) {
    const query = new PS('deleteRide', 'DELETE FROM rides WHERE id = $1');

    query.values = [rideId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

function deleteRideScale(rideScaleId) {
    const query = new PS('deleteRide', 'DELETE FROM rides_scales WHERE id = $1');

    query.values = [rideScaleId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

function getRideScaleInfo(rideScaleId) {
    const query = new PS('getRideInfo', 'SELECT * FROM rides_scales WHERE id = $1');

    query.values = [rideScaleId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

function editRide(ride_id, rideInfo){
    const query = new PS('editRide', 'UPDATE rides SET origin = $1, destination = $2, departure_time = $3,' +
        ' arrival_time = $4, distance = $5 WHERE id = $6 RETURNING id');

    query.values = [rideInfo.origin, rideInfo.destination, rideInfo.startDate,
        rideInfo.arrivalDate, rideInfo.distanceValue, ride_id];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

function editRideScale(scale_id, rideInfo){
    const query = new PS('editScaleRide', 'UPDATE rides SET origin = $1, destination = $2, departure_time = $3,' +
        ' arrival_time = $4, distance = $5 WHERE scale_id = $6 RETURNING id');

    query.values = [rideInfo.origin, rideInfo.destination, rideInfo.startDate,
        rideInfo.arrivalDate, rideInfo.distanceValue, scale_id];

    return db.any(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

module.exports = {
    insertRide : insertRide,
    editRide: editRide,
    insertScale: insertScale,
    deleteScale: deleteScale,
    getRideInfo: getRideInfo,
    deleteRide: deleteRide,
    deleteRideScale: deleteRideScale,
    getRideScaleInfo: getRideScaleInfo,
    editRideScale: editRideScale
};