/**
 * This file contains the utils methods for the rides routes
 */

const distance = require('google-distance');
const Promise = require('bluebird');

/**
 * This function is used to get the information like distance and travel time, between two locations.
 * @param {object} ridesParams - Object with the ride information
 */
function getTravelInfo(ridesParams) {
    return new Promise(function (resolve, reject) {
        let arrival;
        let startDate;

        arrival = new Date(ridesParams.arrivalDate);
        arrival = new Date(arrival + 60 * 60000);

        distance.get(
            {
                origin: ridesParams.origin,
                destination: ridesParams.destination,
                avoid: ridesParams.avoid
            },
            function (err, data) {
                if (err) return reject({success: false, message: err});

                var duration = data.durationValue;

                startDate = new Date(arrival - duration * 1000);

                return resolve({
                    success: true,
                    origin: ridesParams.origin,
                    destination: ridesParams.destination,
                    startDate: startDate,
                    arrivalDate: arrival,
                    duration: data.duration,
                    distance: data.distance,
                    distanceValue: data.distanceValue
                });
            });
    });
}

module.exports = {
    getTravelInfo: getTravelInfo

};