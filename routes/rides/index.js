/**
 * This files declares the rides routes
 */

const Rides = require('express').Router();
const Utils = require('../utils');

var ridesController = require('./ridesController.js');

// Rides routes
Rides.get('/getrideinfo', ridesController.getRideInfo);
Rides.get('/getridescaleinfo', ridesController.getRideScaleInfo);

Rides.post('/createride', ridesController.createRide);
Rides.post('/insertride', ridesController.insertRide);
Rides.post('/createscalerides', ridesController.createScaleOfRides);

Rides.put('/editride', ridesController.editRide);
Rides.put('/editridescale', ridesController.editRideScale);

Rides.delete('/deleteride', ridesController.deleteRide);
Rides.delete('/deleteridescale', ridesController.deleteRideScale);

module.exports = Rides;