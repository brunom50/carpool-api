/**
 * This files declares the groups routes
 */

const Groups = require('express').Router();
const Utils = require('../utils');

var usersController = require('./groupsController.js');

// Groups routes
Groups.get('/getgroupinfo', usersController.getGroupInfo);

Groups.post('/creategroup', usersController.createGroup);

Groups.put('/editgroup', Utils.loginRequired, usersController.editGroup);

Groups.delete('/deletegroup', usersController.deleteGroup);

module.exports = Groups;