/**
 * This file is the controller for the groups routes
 */

const groupsDbMethods = require('./groupsDbMethods');
const groupsMethods = require('./groupsMethods');

/**
 * This function is used to get the info from a rides group chosed by the user
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function getGroupInfo(req, res) {
    let groupId = req.body.groupId;

    if (groupId === undefined) {
        throw new Error('Please insert an group id');
    }

    return groupsDbMethods.getGroupInfo(groupId)
        .then(function (groupInfo) {
            if (groupInfo.error) {
                throw new Error('No group was found with that id');
            }

            return res.status(200).json({success: true, message: groupInfo});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

/**
 * This function is used to create a rides group
 * @param req
 * @param res
 * @returns {*}
 */
function createGroup(req, res) {
    let groupInfo = req.body;

    if (groupInfo.admin === undefined || groupInfo.name === undefined) {
        return res.status(500).json({
            success: false,
            message: 'Required fields missing'
        });
    }

    return groupsDbMethods.createGroup(groupInfo)
        .then(function (result) {
            if (result.id === undefined) {
                throw new Error(result.detail);
            }

            return res.status(200).json({
                success: true,
                message: result
            });
        })
        .catch(function (err) {
            return res.status(500).json({success: false, message: err.message});
        });
}

/**
 * This function is used to update the info of an user
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function editGroup(req, res) {
    let groupInfo = req.body;

    if (groupInfo.id === undefined) {
        throw new Error('Please insert an group id');
    }

    return groupsDbMethods.editGroupInfo(groupInfo)
        .then(function (result) {
            if (result.received === 0) {
                throw new Error('No group with that id was found');
            }

            return res.json({success: true, message: 'Group info of ' + result.id + ' was updated'});
        })
        .catch(function (err) {
            return res.status(500).json({success: false, message: err.message});
        });
}

/**
 * This function is used to delete a rides group selected by the user
 * @param req
 * @param res
 * @returns {Promise.<TResult>}
 */
function deleteGroup(req, res) {
    let groupId = req.body.groupId;

    if (groupId === undefined) {
        throw new Error('Please insert an group id');
    }

    return groupsDbMethods.deleteGroup(groupId)
        .then(function (groupInfo) {
            if (groupInfo.error) {
                throw new Error('No group was found with that id');
            }

            return res.status(200).json({success: true, message: 'The group was deleted'});
        })
        .catch(function (error) {
            return res.status(500).json({success: false, message: error})
        })
}

module.exports = {
    createGroup : createGroup,
    editGroup   : editGroup,
    getGroupInfo: getGroupInfo,
    deleteGroup: deleteGroup
};
