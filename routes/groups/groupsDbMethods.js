/**
 * This file contains the db methods of the groups routes
 */

var promise = require('bluebird');

var options = {
    promiseLib: promise
};

var pgp = require('pg-promise')(options);
const PS = pgp.PreparedStatement;

var connectionString = 'postgres://postgres:root@localhost:5433/carpool';
var db = pgp(connectionString);

/**
 * This function is used to insert a new rides group into the database
 * @param {object} groupInfo - Object with the new group information
 * @returns {Promise.<TResult>}
 */
function createGroup(groupInfo) {
    const query = new PS('addGroup', 'INSERT INTO groups (name, admin, participants)' +
        'VALUES ($1, $2, $3) RETURNING id');

    var params = [groupInfo.name, groupInfo.admin, groupInfo.partipants];

    query.values = params;

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to query the database and update the group info. The query is built dynamically depending of the
 * info that the user wants to update.
 * @param {object} groupInfo - Object containing all the group info that is going to be edited
 * @returns {Promise.<TResult>}
 */
function editGroupInfo(groupInfo) {
    let params = [];
    let query = 'UPDATE groups SET ';
    let regex = /,\s*$/;

    if (groupInfo.name) {
        params.push(groupInfo.name);
        query += 'name' + ' = $' + params.length + ', ';
    }

    if (groupInfo.admin) {
        params.push(groupInfo.admin);
        query += 'admin' + ' = $' + params.length + ', ';
    }

    if (groupInfo.participants) {
        params.push(groupInfo.participants);
        query += 'participants' + ' = $' + params.length + ', ';
    }

    query = query.replace(regex, "");

    params.push(groupInfo.id);
    query += (' WHERE id = $' + params.length + ' RETURNING id');

    const preparedQuery = new PS('editGroupInfo', query);
    preparedQuery.values = params;

    return db.one(preparedQuery)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 * This function is used to query the database and get the info from a rides group
 * @param {string} groupId - Id of the rides group
 * @returns {Promise.<TResult>}
 */
function getGroupInfo(groupId) {
    const query = new PS('getGroupInfo', 'SELECT * FROM groups WHERE id = $1');

    query.values = [groupId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

/**
 *
 * @param {string} groupId - Id of the rides group that the user wants to delete
 * @returns {Promise.<TResult>}
 */
function deleteGroup(groupId) {
    const query = new PS('getGroupInfo', 'DELETE FROM groups WHERE id = $1');

    query.values = [groupId];

    return db.one(query)
        .then((result) => {
            return result;
        })
        .catch(error => {
            return error;
        });
}

module.exports = {
    createGroup  : createGroup,
    editGroupInfo: editGroupInfo,
    getGroupInfo : getGroupInfo,
    deleteGroup: deleteGroup
};